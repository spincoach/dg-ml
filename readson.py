import json
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
from sklearn import svm

def pullLength(X, freq):
    threshold = 0.2*(np.max(X)) #modify<-
    len_s = 0
    for x in X:
        if x > threshold:
            len_s += float(1)/float(freq)#len_s + (1/freq)
            
    return float(len_s)

def smooth(X):
    smooth = np.copy(X)
    smooth = savgol_filter(X, 13, 3)
    return smooth

def bumpiness(X):
    X_smooth = smooth(X)
    residual = X_smooth-X
    ssq = np.sum(residual**2)
    #plt.plot(X)
    #plt.plot(X_smooth)
    
    return ssq

# fcns for slicing
def getStart(X):
    midx = np.argmax(X)
    startidx = midx - 25
    endidx = midx + 10
    return [startidx, endidx]

def getSample(X):
    s = getStart(X)
    smpl = X[s[0]: s[1]]
    return smpl

freq = 52 # hz

#%% Start reading data section
directory = os.fsencode('data')

data = []

for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".json"):
        print("Read" ,filename)
        fstring = "data/" + filename
        data.append(json.load(open(fstring)))


        continue
    else:
        continue


y = [] # dependent var
sd = [] # independent var's

# split into input and output data
for d in data:
    y.append(d["grade"])
    sd.append(d["sensorData"])

# get alleuclidean acceleration vectors into a_tot array
    # a_tot[0] is now a vector
a_tot= []
i = 0
j= 0
for inst in sd:
    tmp = []
    for valset in inst:
        
        acc = valset["acceleration"]
        tmp.append(np.sqrt(acc["x"]**2 + acc["x"]**2 + acc["x"]**2))

        j += 1
    a_tot.append(tmp)
    i+=1
    
    
#%% extract features from vectors, put these in 

i = 0
x_bumpiness = []
x_pull_length =[]
X=[]

for aa in a_tot:
    S = getSample(aa)
    x_pull_length.append(pullLength(S, freq))
    x_bumpiness.append(bumpiness(S))
    
    '''
    if y[i] == 'short':
        plt.plot(aa, color = 'r')
    elif y[i] == 'bumpy':
        plt.plot(aa, color = 'k')
    elif y[i] == 'good':
        plt.plot(aa, color = 'green') 
      '''  
    i = i+1
    #if i > 2:
    #   break
    
#%% make y variants
y_n = []
y_c = []

for r in y:
    if r == 'short':
        y_c.append('red') 
        y_n.append(1)
    elif r == 'bumpy':
        y_c.append('orange') 
        y_n.append(2)
    elif r == 'good':
        y_c.append('green') 
        y_n.append(3)


#%%
X=[]        
x_bumpiness /= np.max(np.abs(x_bumpiness),axis=0)
x_pull_length /= np.max(np.abs(x_pull_length),axis=0)
X=[x_bumpiness, x_pull_length]
X=list(zip(*X))
#%% plot var's
plt.scatter(x_bumpiness,x_pull_length, c=y_c)
filters = {'short': 1, 'bumpy':2, 'good':3}
#%% Start TF stuff



import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from sklearn import datasets
from tensorflow.python.framework import ops
ops.reset_default_graph()

# Create graph
sess = tf.Session()

# Load the data
# iris.data = [(Sepal Length, Sepal Width, Petal Length, Petal Width)]

x_vals = np.array(X)
y_vals1 = np.array([1 if y==1 else -1 for y in y_n])
y_vals2 = np.array([1 if y==2 else -1 for y in y_n])
y_vals3 = np.array([1 if y==3 else -1 for y in y_n])
y_vals = np.array([y_vals1, y_vals2, y_vals3])

# Declare batch size
batch_size = 40

# Initialize placeholders
x_data = tf.placeholder(shape=[None, 2], dtype=tf.float32)
y_target = tf.placeholder(shape=[3, None], dtype=tf.float32)
prediction_grid = tf.placeholder(shape=[None, 2], dtype=tf.float32)

# Create variables for svm
b = tf.Variable(tf.random_normal(shape=[3,batch_size]))

# Gaussian (RBF) kernel
#gamma = tf.constant(-10.0)
gamma = tf.constant(-20.0)
dist = tf.reduce_sum(tf.square(x_data), 1)
dist = tf.reshape(dist, [-1,1])
sq_dists = tf.multiply(2., tf.matmul(x_data, tf.transpose(x_data)))
my_kernel = tf.exp(tf.multiply(gamma, tf.abs(sq_dists)))

# Declare function to do reshape/batch multiplication
def reshape_matmul(mat):
    v1 = tf.expand_dims(mat, 1)
    v2 = tf.reshape(v1, [3, batch_size, 1])
    return(tf.matmul(v2, v1))

# Compute SVM Model
first_term = tf.reduce_sum(b)
b_vec_cross = tf.matmul(tf.transpose(b), b)
y_target_cross = reshape_matmul(y_target)

second_term = tf.reduce_sum(tf.multiply(my_kernel, tf.multiply(b_vec_cross, y_target_cross)),[1,2])
loss = tf.reduce_sum(tf.negative(tf.subtract(first_term, second_term)))

# Gaussian (RBF) prediction kernel
rA = tf.reshape(tf.reduce_sum(tf.square(x_data), 1),[-1,1])
rB = tf.reshape(tf.reduce_sum(tf.square(prediction_grid), 1),[-1,1])
pred_sq_dist = tf.add(tf.subtract(rA, tf.multiply(2., tf.matmul(x_data, tf.transpose(prediction_grid)))), tf.transpose(rB))
pred_kernel = tf.exp(tf.multiply(gamma, tf.abs(pred_sq_dist)))

prediction_output = tf.matmul(tf.multiply(y_target,b), pred_kernel)
prediction = tf.arg_max(prediction_output-tf.expand_dims(tf.reduce_mean(prediction_output,1), 1), 0)
accuracy = tf.reduce_mean(tf.cast(tf.equal(prediction, tf.argmax(y_target,0)), tf.float32))

# Declare optimizer
my_opt = tf.train.GradientDescentOptimizer(0.01)
train_step = my_opt.minimize(loss)

# Initialize variables
init = tf.global_variables_initializer()
sess.run(init)

# Training loop
loss_vec = []
batch_accuracy = []
for i in range(1000):
    rand_index = np.random.choice(len(x_vals), size=batch_size)
    rand_x = x_vals[rand_index]
    rand_y = y_vals[:,rand_index]
    sess.run(train_step, feed_dict={x_data: rand_x, y_target: rand_y})
    
    temp_loss = sess.run(loss, feed_dict={x_data: rand_x, y_target: rand_y})
    loss_vec.append(temp_loss)
    
    acc_temp = sess.run(accuracy, feed_dict={x_data: rand_x,
                                             y_target: rand_y,
                                             prediction_grid:rand_x})
    batch_accuracy.append(acc_temp)
    
    if (i+1)%25==0:
        print('Step #' + str(i+1))
        print('Loss = ' + str(temp_loss))

# Create a mesh to plot points in
x_min, x_max = x_vals[:, 0].min() - 1, x_vals[:, 0].max() + 1
y_min, y_max = x_vals[:, 1].min() - 1, x_vals[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.02),
                     np.arange(y_min, y_max, 0.02))
grid_points = np.c_[xx.ravel(), yy.ravel()]
grid_predictions = sess.run(prediction, feed_dict={x_data: rand_x,
                                                   y_target: rand_y,
                                                   prediction_grid: grid_points})
grid_predictions = grid_predictions.reshape(xx.shape)


# Plot points and grid
plt.contourf(xx, yy, grid_predictions, cmap=plt.cm.Paired, alpha=0.8)
plt.scatter(X[:,[0]], X[:,[1]], c=y_c)
#plt.plot(class1_x, class1_y, 'ro', label='I. setosa')
#plt.plot(class2_x, class2_y, 'kx', label='I. versicolor')
#plt.plot(class3_x, class3_y, 'gv', label='I. virginica')
plt.title('Gaussian SVM Results on Iris Data')
plt.xlabel('Bumpiness of throw')
plt.ylabel('Length of throw')
plt.legend(loc='lower right')
plt.show()

# Plot batch accuracy
plt.plot(batch_accuracy, 'k-', label='Accuracy')
plt.title('Batch Accuracy')
plt.xlabel('Generation')
plt.ylabel('Accuracy')
plt.legend(loc='lower right')
plt.show()

# Plot loss over time
plt.plot(loss_vec, 'k-')
plt.title('Loss per Generation')
plt.xlabel('Generation')
plt.ylabel('Loss')
plt.show()
